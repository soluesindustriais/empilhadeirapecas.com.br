<?
$h1         = 'Empilhadeira';
$title      = 'Página Inicial';
$desc       = 'Se procura por '.$h1.', você encontra nos resultados do Soluções Industriais, receba diversos orçamentos com mais de 100 empresas do Brasil ao';
$var        = 'Home';
include('inc/head.php');
?>
</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero">
	<div class="title-main"> <h1>Empilhadeira</h1> </div>
	<ul class="cd-hero-slider autoplay">
		<li class="selected">
			<div class="cd-full-width">
				<h2>Empilhadeira Elétrica</h2>
				<p>A empilhadeira elétrica paletrans é um equipamento muito utilizado no atual mercado, por empresas de nichos variados, para realizar o transporte de cargas variadas. </p>
				<a href="<?=$url?>empilhadeira-eletrica" class="cd-btn">Saiba mais</a>
			</div>
		</li>
		<li>
			<div class="cd-full-width">
				<h2>Manutenção de Empilhadeiras</h2>
				<p>Assim como qualquer outro material é importante realizar a manutenção de empilhadeiras periódicamente, que podem ser realizadas de dois modos. O primeiro tipo é a...</p>
				<a href="<?=$url?>manutencao-de-empilhadeiras" class="cd-btn">Saiba mais</a>
			</div>
		</li>
		<li>
			<div class="cd-full-width">
				<h2>Aluguel de Empilhadeira</h2>
				<p>É um serviço fundamental que trará muitos benefícios à empresa contratante, uma vez que vários empreendimentos transportam diversos equipamentos o tempo todo. O aluguel evita que as empresas...</p>
				<a href="<?=$url?>aluguel-de-empilhadeira" class="cd-btn">Saiba mais</a>
			</div>
		</li>
	</ul>
	<div class="cd-slider-nav">
		<nav>
			<span class="cd-marker item-1"></span>
			<ul>
				<li class="selected"><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
				<li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
				<li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
			</ul>
		</nav>
	</div>
</section>
<main>
	<section class="wrapper-main">
		<div class="main-center">
			<div class="quadro-2">
				<h2 class="border-left">Empilhadeira</h2>
				<div class="div-img">
					<p>
					Se pesquisa por Acionamento da peças para empilhadeira, você só obtém nas buscas do Soluções Industriais, receba os valores médios já com aproximadamente 100 empresas de todo o Brasil gratuitamente para todo o Brasil.
					</p>
				</div>
				<div class="gerador-svg" >
					<img src="imagens/locacao/locacao-de-empilhadeira-12.jpg" alt="Locação de Empilhadeira" title="Locação de Empilhadeira">
				</div>
			</div>
			<div class=" incomplete-box">
				<h2>Onde utilizar este produto</h2>
				<p>
				Receba uma cotação de Cesta de natal, você consegue nos resultados do Soluções Industriais, faça um orçamento online com aproximadamente 500 distribuidores gratuitamente para todo o Brasil:
				</p>

				<ul>
					<li style="color: #ffce00;"><a href="<?=$url?>aluguel-de-empilhadeira"><i class="fas fa-long-arrow-alt-right"></i> Aluguel de Empilhadeira  </a></li>
					<li style="color: #ffce00;"><a href="<?=$url?>empilhadeiras-manuais"><i class="fas fa-long-arrow-alt-right"></i> Empilhadeiras Manuais  </a></li>
					<li style="color: #ffce00;"><a href="<?=$url?>locacao-de-empilhadeira"><i class="fas fa-long-arrow-alt-right"></i> Locação de Empilhadeira  </a></li>
					<li style="color: #ffce00;"><a href="<?=$url?>manutencao-empilhadeira"><i class="fas fa-long-arrow-alt-right"></i> Manutenção Empilhadeira </a></li>
					<li style="color: #ffce00;"><a href="<?=$url?>pneu-para-empilhadeira"><i class="fas fa-long-arrow-alt-right"></i> Pneu para Empilhadeira  </a></li>
					<li style="color: #ffce00;"><a href="<?=$url?>empilhadeira-eletrica"><i class="fas fa-long-arrow-alt-right"></i> Empilhadeira Elétrica  </a></li>
					<li style="color: #ffce00;"><a href="<?=$url?>manutencao-de-empilhadeiras"><i class="fas fa-long-arrow-alt-right"></i> Manutenção de Empilhdeiras  </a></li>
					<li style="color: #ffce00;"><a href="<?=$url?>empilhadeira-retratil"><i class="fas fa-long-arrow-alt-right"></i> Empilhadeira Retrátil  </a></li>
					
					
				</ul>
				<a href="<?$url?>produtos">
					<span class="btn-4" > Orçamento Grátis </span>
				</a>
			</div>
		</div>
		<div id="content-icons">
			<div class="co-icon">
				<div class="quadro-icons">
					<i class="fas fa-building fa-7x"></i>
					<div>
						<p>Cote com diversas empresas</p>
					</div>
				</div>
			</div>
			<div class="co-icon">
				<div class="quadro-icons" >
					<i class="fas fa-dollar-sign fa-7x"></i>
					<div>
						<p>Compare preços</p>
					</div>
				</div>
			</div>
			<div class="co-icon">
				<div class="quadro-icons" >
					<i class="fas fa-handshake fa-7x"></i>
					<div>
						<p>Faça o melhor negócio</p>
					</div>
				</div>
			</div>
			<div class="co-icon">
				<div class="quadro-icons" >
					<i class="fas fa-clock fa-7x"></i>
					<div>
						<p>Economize tempo</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="wrapper-img">
		<div class="txtcenter">
			<h2 class="border-left" >Produtos Relacionados</h2>
		</div>
		<div class="content-icons">
			<div class="produtos-relacionados-1">
				<figure>
					<a href="<?=$url?>aluguel-empilhadeira">
						<div class="fig-img">
							<h2>ALUGUEL EMPILHADEIRA</h2>
							<div class="saiba-mais">
								<span class="btn-saiba-mais">Saiba mais</span>
							</div>
						</div>
					</a>
				</figure>
			</div>
			
			<div class="produtos-relacionados-2">
				<figure class="figure2">
					<a href="<?=$url?>pecas-para-paleteira-manual">
						<div class="fig-img2">
							<h2 class="concerto-maquina">PEÇAS PARA PALETEIRA MANUAL</h2>
							<div class="saiba-mais-2">
								<span class="btn-saiba-mais">Saiba mais</span>
							</div>
						</div>
					</a>
				</figure>
			</div>
			<div class="produtos-relacionados-3">
				<figure>
					<a href="<?=$url?>empilhadeira-eletrica">
						<div class="fig-img">
							<h2>Empilhadeira Elétrica</h2>
							<div class="saiba-mais">
								<span class="btn-saiba-mais">Saiba mais</span>
							</div>
						</div>
					</a>
				</figure>
			</div>
		</div>
	</section>
	<section class="wrapper-destaque">
		<div class="destaque txtcenter">
			<h2 class="border-left">Galeria de Produtos</h2>
			<div class="center-block txtcenter">
				<ul class="gallery">
					<li>
						<a href="<?=$url?>imagens/transmissao-1.jpg" class="lightbox" title="Transmissão">
							<img src="<?=$url?>imagens/thumbs/transmissao-1.jpg" title="Transmissão" alt="Transmissão">
						</a>
					</li>
					<li>
						<a href="<?=$url?>imagens/transmissao-7.jpg" class="lightbox"  title="Transmissão">
							<img src="<?=$url?>imagens/thumbs/transmissao-7.jpg" alt="Transmissão" title="Transmissão">
						</a>
					</li>
					<li>
						<a href="<?=$url?>imagens/sistema-eletrico-3.jpg" class="lightbox" title="Sistema elétrico">
							<img src="<?=$url?>imagens/thumbs/sistema-eletrico-3.jpg" alt="Sistema elétrico" title="Sistema elétrico">
						</a>
					</li>
					<li>
						<a href="<?=$url?>imagens/sistema-de-freios-1.jpg" class="lightbox" title="Sistema de freios">
							<img src="<?=$url?>imagens/thumbs/sistema-de-freios-1.jpg" alt="Sistema de freios" title="Sistema de freios">
						</a>
					</li>
					<li>
						<a href="<?=$url?>imagens/sistema-de-direcao-2.jpg" class="lightbox" title="Sistema de direção">
							<img src="<?=$url?>imagens/thumbs/sistema-de-direcao-2.jpg" alt="Sistema de direção"  title="Sistema de direção">
						</a>
					</li>
					<li>
						<a href="<?=$url?>imagens/opcionais-3.jpg" class="lightbox" title="opcionais">
							<img src="<?=$url?>imagens/thumbs/opcionais-3.jpg" alt="opcionais" title="opcionais">
						</a>
					</li>
					<li>
						<a href="<?=$url?>imagens/motor-4.jpg" class="lightbox" title="Motor">
							<img src="<?=$url?>imagens/thumbs/motor-4.jpg" alt="Motor" title="Motor">
						</a>
					</li>
					<li>
						<a href="<?=$url?>imagens/combustivel-8.jpg" class="lightbox" title="Combustível">
							<img src="<?=$url?>imagens/thumbs/combustivel-8.jpg" alt="Combustível" title="Combustível">
						</a>
					</li>
					<li>
						<a href="<?=$url?>imagens/combustivel-9.jpg" class="lightbox" title="Combustível">
							<img src="<?=$url?>imagens/thumbs/combustivel-9.jpg" alt="Combustível" title="Combustível">
						</a>
					</li>
					<li>
						<a href="<?=$url?>imagens/combustivel-10.jpg" class="lightbox" title="Combustível">
							<img src="<?=$url?>imagens/thumbs/combustivel-10.jpg" alt="Combustível" title="Combustível">
						</a>
					</li>
				</ul>
			</div>
		</div>
	</section>
</main>
<?
	include('inc/footer.php');
	include('inc/fancy.php');
?>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>
</body>
</html>
