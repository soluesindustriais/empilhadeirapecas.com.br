<?
	$h1 = "Produtos";
	$title = "Produtos";
	$desc = "Encontre diversos fornecedores de grama sintética e quadras poliesportivas. Clique aqui para saber mais detalhes. Dúvidas, entre em contato conosco agora mesmo";
	$var = "Produtos";
	include('inc/head.php');
?>
	</head>
	<body>
	<? include('inc/topo.php');?>
	<div class="wrapper">
	 	<main>
	 		<div class="content">
	 			<div id="breadcrumb" itemscope itemtype="https://data-vocabulary.org/breadcrumb">
	 				<a rel="home" itemprop="url" href="<?=$url?>" title="home">
	 					<span itemprop="title">
	 						<i class="fa fa-home" aria-hidden="true"></i>Home
	 					</span>
	 				</a> »
					<strong><span class="page" itemprop="title">Produtos</span></strong>
	 			</div>


				<h1>Produtos</h1>
				<article class="full">
					<p>Encontre diversos fornecedores de grama sintética e quadras poliesportivas, cote agora mesmo!</p>
					<ul class="thumbnails-main">
						<li>
							<a rel="nofollow" href="<?=$url?>locacao-categoria" title="Locação">
								<img src="<?$url?>imagens/locacao/locacao-de-empilhadeira-1.jpg" alt="Locação" title="Locação"/>
							</a>
							<h2>
								<a href="<?=$url?>locacao-categoria" title="Locação">
									Locação
								</a>
							</h2>
						</li>

						<li>
							<a rel="nofollow" href="<?=$url?>manutencao-categoria" title="Manutenção">
								<img src="<?$url?>imagens/manutencao/manutencao-de-empilhadeira-1.jpg" alt="Manutenção" title="Manutenção"/>
							</a>
							<h2>
								<a href="<?=$url?>manutencao-categoria" title="Manutenção">
									Manutenção
								</a>
							</h2>
						</li>

						<li>
							<a rel="nofollow" href="<?=$url?>pecas-categoria" title="Peças">
								<img src="<?$url?>imagens/pecas/pecas-de-empilhadeira-1.jpg" alt="Peças" title="Peças"/>
							</a>
							<h2>
								<a href="<?=$url?>pecas-categoria" title="Peças">
									Peças
								</a>
							</h2>
						</li>

						<li>
							<a rel="nofollow" href="<?=$url?>venda-categoria" title="Venda">
								<img src="<?$url?>imagens/venda/venda-de-empilhadeira-1.jpg" alt="Venda" title="Venda"/>
							</a>
							<h2>
								<a href="<?=$url?>venda-categoria" title="Venda">
									Venda
								</a>
							</h2>
						</li>
					</ul>
				</article>
	 		</div>
	 	</main>
	 </div>
	 <? include('inc/footer.php');?>
	</body>
</html>
