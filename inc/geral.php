<?
$nomeSite			= 'Portal da Empilhadeira';
$slogan				= 'Produtos e serviços de Empilhadeira você encontra aqui!';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") { $url = $http."://".$host."/"; }
else { $url = $http."://".$host.$dir["dirname"]."/";  }


$emailContato		= 'everton.lima@solucoesindustriais.com.br';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$idAnalytics		= 'UA-121693798-32';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php','',$urlPagina);
$urlPagina 			== "index"? $urlPagina= "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
//Breadcrumbs

$caminho  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminho .= '<a rel="home" itemprop="url" href="'.$url.'" title="Home">';
$caminho .= '<span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminho .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div>';

$caminho2	 = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminho2	.= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminho2	.= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminho2	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminho2	.= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminho2	.= '<span itemprop="title"> Produtos </span></a> »';
$caminho2	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminho2	.= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div>';



$caminhovenda  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhovenda .= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminhovenda .= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminhovenda .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhovenda .= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhovenda .= '<span itemprop="title"> Produtos </span></a> »';
$caminhovenda .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhovenda .= '<a href="'.$url.'venda-categoria" title="Categoria - Venda" class="category" itemprop="url">';
$caminhovenda .= '<span itemprop="title"> Categoria - Venda </span></a> »';
$caminhovenda .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhovenda .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div></div>';

$caminhomanutencao  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhomanutencao .= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminhomanutencao .= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminhomanutencao .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhomanutencao .= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhomanutencao .= '<span itemprop="title"> Produtos </span></a> »';
$caminhomanutencao .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhomanutencao .= '<a href="'.$url.'manutencao-categoria" title="Categoria - Manutenção" class="category" itemprop="url">';
$caminhomanutencao .= '<span itemprop="title"> Categoria - Manutenção </span></a> »';
$caminhomanutencao .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhomanutencao .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div></div>';

$caminholocacao  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminholocacao .= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminholocacao .= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminholocacao .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminholocacao .= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminholocacao .= '<span itemprop="title"> Produtos </span></a> »';
$caminholocacao .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminholocacao .= '<a href="'.$url.'locacao-categoria" title="Categoria - Locação" class="category" itemprop="url">';
$caminholocacao .= '<span itemprop="title"> Categoria - Locação </span></a> »';
$caminholocacao .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminholocacao .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div></div>';

$caminhopecas  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhopecas .= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminhopecas .= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminhopecas .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhopecas .= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhopecas .= '<span itemprop="title"> Produtos </span></a> »';
$caminhopecas .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhopecas .= '<a href="'.$url.'pecas-categoria" title="Categoria - Peças" class="category" itemprop="url">';
$caminhopecas .= '<span itemprop="title"> Categoria - Peças </span></a> »';
$caminhopecas .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhopecas .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div></div>';


/**$caminhoinformacoes  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhoinformacoes .= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminhoinformacoes .= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminhoinformacoes .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhoinformacoes .= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhoinformacoes .= '<span itemprop="title"> Produtos </span></a> »';
$caminhoinformacoes .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhoinformacoes .= '<a href="'.$url.'informacoes" title="Categoria - informações" class="category" itemprop="url">';
$caminhoinformacoes .= '<span itemprop="title"> Informações </span></a> »';
$caminhoinformacoes .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhoinformacoes .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div></div>';**/